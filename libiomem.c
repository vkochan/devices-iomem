#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define PAGE_SIZE 4096UL
#define PAGE_MASK (PAGE_SIZE - 1)

long iomem_map(unsigned long addr, unsigned long size)
{
    void *map;
    int fd;

    fd = open("/dev/mem", O_RDWR | O_SYNC);
    if (fd == -1)
        return -1;
    map = mmap(0, (size + PAGE_MASK) & ~PAGE_MASK, PROT_READ | PROT_WRITE, MAP_SHARED, fd, addr & ~PAGE_MASK);
    if (map == (void *) -1) {
        close(fd);
        return -1;
    };

    close(fd);
    return (long)(map + (addr & PAGE_MASK));
};

void iomem_unmap(unsigned long addr, unsigned long size)
{
    munmap(addr & ~PAGE_MASK, (size + PAGE_MASK) & ~PAGE_MASK);
};
