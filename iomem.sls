;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2024 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(library (devices iomem)
  (export
    map-iomem
    unmap-iomem

    iomem-ptr
    iomem-size

    iomem-ref-u8
    iomem-ref-s8
    iomem-ref-u16
    iomem-ref-s16
    iomem-ref-u32
    iomem-ref-s32
    iomem-ref-u64
    iomem-ref-s64
    iomem-set-u8
    iomem-set-s8
    iomem-set-u16
    iomem-set-s16
    iomem-set-u32
    iomem-set-s32
    iomem-set-u64
    iomem-set-s64)
  (import (rnrs)
          (pffi))

(define-record-type iomem
  (fields ptr size))

(define (map-iomem addr size)
  (let ([ret (ffi-iomem-map addr size)])
    (when (< ret 0)
      (error 'map-iomem "Could not map address" addr size))
    (make-iomem (integer->pointer ret) size)))

(define (unmap-iomem mem)
  (ffi-iomem-unmap (pointer->integer (iomem-ptr mem))
                   (iomem-size mem)))

(define (iomem-ref-u8 mem offs)
  (pointer-ref-c-uint8 (iomem-ptr mem) offs))

(define (iomem-ref-s8 mem offs)
  (pointer-ref-c-int8 (iomem-ptr mem) offs))

(define (iomem-ref-u16 mem offs)
  (pointer-ref-c-uint16 (iomem-ptr mem) offs))

(define (iomem-ref-s16 mem offs)
  (pointer-ref-c-int16 (iomem-ptr mem) offs))

(define (iomem-ref-u32 mem offs)
  (pointer-ref-c-uint32 (iomem-ptr mem) offs))

(define (iomem-ref-s32 mem offs)
  (pointer-ref-c-int32 (iomem-ptr mem) offs))

(define (iomem-ref-u64 mem offs)
  (pointer-ref-c-uint64 (iomem-ptr mem) offs))

(define (iomem-ref-s64 mem offs)
  (pointer-ref-c-int64 (iomem-ptr mem) offs))

(define (iomem-set-u8 mem offs val)
  (pointer-set-c-uint8! (iomem-ptr mem) offs val))

(define (iomem-set-s8 mem offs val)
  (pointer-set-c-int8! (iomem-ptr mem) offs val))

(define (iomem-set-u16 mem offs val)
  (pointer-set-c-uint16! (iomem-ptr mem) offs val))

(define (iomem-set-s16 mem offs val)
  (pointer-set-c-int16! (iomem-ptr mem) offs val))

(define (iomem-set-u32 mem offs val)
  (pointer-set-c-uint32! (iomem-ptr mem) offs val))

(define (iomem-set-s32 mem offs val)
  (pointer-set-c-int32! (iomem-ptr mem) offs val))

(define (iomem-set-u64 mem offs val)
  (pointer-set-c-uint32! (iomem-ptr mem) offs val))

(define (iomem-set-s64 mem offs val)
  (pointer-set-c-int32! (iomem-ptr mem) offs val))

(define libiomem (open-shared-object "libiomem.so"))

(define ffi-iomem-map (foreign-procedure libiomem long iomem_map (unsigned-long unsigned-long)))
(define ffi-iomem-unmap (foreign-procedure libiomem void iomem_unmap (unsigned-long unsigned-long)))
)
